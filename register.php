<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tarea1 - Taller de integración de software</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg">

        <div class="home-btn d-none d-sm-block">
            <a href="index.html"><i class="fas fa-home h2 text-dark"></i></a>
        </div>

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="text-center">
                            <!-- <a href="index.html">
                                <span><img src="assets/images/logo-dark.png" alt="" height="22"></span>
                            </a>
                            <p class="text-muted mt-2 mb-4">Responsive Admin Dashboard</p> -->
                        </div>
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center mb-4">
                                    <h4 class="text-uppercase mt-0">Registro</h4>
                                </div>

                                 <!-- <form action="#"> -->

                                    <div class="form-group">
                                        <label for="fullname">RUT</label>
                                        <input class="form-control" type="text" id="txt_rut" placeholder="Ingrerse su RUT" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="emailaddress">Email</label>
                                        <input class="form-control" type="email" id="emailaddress" required placeholder="Ingrese su e-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Contraseña</label>
                                        <input class="form-control" type="password" required id="password" placeholder="Ingrese su contraseña">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
											<p align="center" class="text-muted" id="msgerror"></p>
                                            <!-- <input type="checkbox" class="custom-control-input" id="checkbox-signup">
                                            <label class="custom-control-label" for="checkbox-signup">Acepto los <a href="javascript: void(0);" class="text-dark">terminos y condiciones</a></label> -->
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 text-center">
                                        <button id="btnvalida" class="btn btn-primary btn-block" > Registrarse </button>
                                    </div>

                                <!-- </form> -->

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <!-- <div class="col-12 text-center">
                                <p class="text-muted">Already have account?  <a href="pages-login.html" class="text-dark ml-1"><b>Sign In</b></a></p>
                            </div> -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="assets/js/vendor.min.js"></script>

        <!-- App js-->
        <script src="assets/js/app.min.js"></script>
        <script>
			
				var Fn = {
				// Valida el rut con su cadena completa "XXXXXXXX-X"
				validaRut : function (rutCompleto) {
					rutCompleto = rutCompleto.replace("‐","-");
					if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
						return false;
					var tmp 	= rutCompleto.split('-');
					var digv	= tmp[1]; 
					var rut 	= tmp[0];
					if ( digv == 'K' ) digv = 'k' ;
					
					return (Fn.dv(rut) == digv );
				},
				dv : function(T){
					var M=0,S=1;
					for(;T;T=Math.floor(T/10))
						S=(S+T%10*(9-M++%6))%11;
					return S?S-1:'k';
				}
			}


			$("#btnvalida").click(function(){
				
				if (Fn.validaRut( $("#txt_rut").val() )){
					$('#emailaddress').focus();
					$("#msgerror").html("");
				} else if($("#txt_rut").val()==''){
					alert("Favor ingrese su rut");
					$('#txt_rut').focus();
				}else{
					$("#msgerror").html("El RUT ingresado no es válido");
				}
			});
			
		</script>
    </body>
</html>