<?php 
include("../../../conexion/conexion.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Sistema Educacional</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Educacion, Estudio, Sistema Educacional, Evaluaciones en linea, Rendimiento escolar" name="description" />
        <meta content="Jean Tapia R." name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico">

        <!-- App css -->
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
    
                        <li class="d-none d-sm-block">
                            <form class="app-search">
                                <div class="app-search-box">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar...">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fe-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
            
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">9</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
    
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-dark">
                                                <small>Limpiar Todo</small>
                                            </a>
                                        </span>Notificaciones
                                    </h5>
                                </div>
    
                                <div class="slimscroll noti-scroll">
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon">
                                            <img src="../../../assets/images/users/user-1.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Cristina Pride</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Hi, How are you? What about our next meeting</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-primary">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">1 min ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon">
                                            <img src="../../../assets/images/users/user-4.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Karen Robinson</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Wow ! this admin looks good and awesome design</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning">
                                            <i class="mdi mdi-account-plus"></i>
                                        </div>
                                        <p class="notify-details">New user registered.
                                            <small class="text-muted">5 hours ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">4 days ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-secondary">
                                            <i class="mdi mdi-heart"></i>
                                        </div>
                                        <p class="notify-details">Carlos Crouch liked
                                            <b>Admin</b>
                                            <small class="text-muted">13 days ago</small>
                                        </p>
                                    </a>
                                </div>
    
                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    Ver Todo
                                    <i class="fi-arrow-right"></i>
                                </a>
    
                            </div>
                        </li>
    
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="../../../assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    <?php echo 'Administrador';?> <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Bienvenido !</h6>
                                </div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>Mi Cuenta</span>
                                </a>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-settings"></i>
                                    <span>Configuraciones</span>
                                </a>

                                <div class="dropdown-divider"></div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Cerrar Sesion</span>
                                </a>
    
                            </div>
                        </li>    
                    </ul>
    
                    <!-- LOGO -->
                    <div class="logo-box">
                        <!-- <a href="index.html" class="logo text-center">
                            <span class="logo-lg">
                                <img src="../../../assets/images/logo-light.png" alt="" height="16">
                            </span>
                            <span class="logo-sm">
                                <img src="../../../assets/images/logo-sm.png" alt="" height="24">
                            </span>
                        </a> -->
                    </div>
    
                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="../"><i class="mdi mdi-view-dashboard"></i>Dashboard</a>
                            </li>
							<li class="has-submenu active">
                                <a href="#"><i class="mdi mdi-monitor-dashboard"></i>Monitor</a>
                            </li>
							<li class="has-submenu">
                                <a href="../establecimientos/"><i class="mdi mdi-home-group"></i>Establecimientos</a>
                            </li>
							<!--
							<li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-lifebuoy"></i>Curriculum <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Editar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Eliminar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                        
                                </ul>
                            </li>
                            
                            <li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-home-group"></i>Establecimiento <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="mdi mdi-account-multiple-outline"></i>Usuarios <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Editar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Eliminar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>
							-->
                            <li class="has-submenu">
                                
                            </li>

                            <li class="has-submenu">
                                
                            </li>

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sistema Educacional</a></li>
                                    <li class="breadcrumb-item active">Monitor</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Monitor</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title --> 

                <div class="row">

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Establecimientos</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Usuarios</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Planificaciones</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Evaluaciones</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-xl-12">
						<div class="card-box table-responsive">
						<table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap">
							<thead>
							<tr>
								<th>Modulo</th>
								<th>Evento</th>
								<th>URL</th>
								<th>Session ID</th>
								<th>Fecha Ejecución</th>
								<th>Resultado</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>Banco de Planificaciones</td>
								<td>Actualizar</td>
								<td>http://localhost/jean_tapia_tarea1/home/banco/planificaciones/update.php</td>
								<td>4DSWQ124S</td>
								<td>16/05/2020 10:11:32</td>
								<td>Actualizacion sin problemas para el registro: #13021, de Nivel: 1 Basico a Nivel: 2 Basico.</td>
							</tr>
							<tr>
								<td>Banco de Recursos</td>
								<td>Agregar</td>
								<td>http://localhost/jean_tapia_tarea1/home/banco/recursos/insert.php</td>
								<td>80OK867YC</td>
								<td>11/05/2020 11:30:32</td>
								<td>Insercion de recursos a plataforma, archivo audivisual: Lenguaje_Senas.ppt</td>
							</tr>
							<tr>
								<td>Matricula</td>
								<td>Eliminar</td>
								<td>http://localhost/jean_tapia_tarea1/home/matriculas/delete.php</td>
								<td>50UJ73J23L</td>
								<td>11/05/2020 16:35:32</td>
								<td>Se elimina el registro #1209 Alumno: Jose Miguel Hurtado - 1° Medio A</td>
							</tr>
							</tbody>
						</table>
						</div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->      

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        2016 - 2019 &copy; Adminto theme by <a href="">Coderthemes</a> 
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="../../../assets/js/vendor.min.js"></script>

        <!-- knob plugin -->
        <script src="../../../assets/libs/jquery-knob/jquery.knob.min.js"></script>
		
		<!-- third party js -->
        <script src="../../../assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.bootstrap4.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="../../../assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.buttons.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.html5.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.flash.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.print.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.keyTable.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.select.min.js"></script>
		

		 <!-- Datatables init -->
        <script src="../../../assets/js/pages/datatables.init.js"></script>

        <!-- App js-->
        <script src="../../../assets/js/app.min.js"></script>
        
    </body>
</html>