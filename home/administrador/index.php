<?php 
include("../../conexion/conexion.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Sistema Educacional</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Educacion, Estudio, Sistema Educacional, Evaluaciones en linea, Rendimiento escolar" name="description" />
        <meta content="Jean Tapia R." name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="../../assets/images/favicon.ico">

        <!--Morris Chart-->
        <link rel="stylesheet" href="../../assets/libs/morris-js/morris.css" />

        <!-- App css -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
    
                        <li class="d-none d-sm-block">
                            <form class="app-search">
                                <div class="app-search-box">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar...">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fe-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
						<li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="modal" data-target=".bs-example-modal-center" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fas fa-life-ring noti-icon"></i>
                            </a>
                        </li>
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fas fa-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">9</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
    
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-dark">
                                                <small>Limpiar Todo</small>
                                            </a>
                                        </span>Notificaciones
                                    </h5>
                                </div>
    
                                <div class="slimscroll noti-scroll">
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon">
                                            <img src="../../assets/images/users/user-1.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Cristina Pride</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Hi, How are you? What about our next meeting</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-primary">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">1 min ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon">
                                            <img src="../../assets/images/users/user-4.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Karen Robinson</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Wow ! this admin looks good and awesome design</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning">
                                            <i class="mdi mdi-account-plus"></i>
                                        </div>
                                        <p class="notify-details">New user registered.
                                            <small class="text-muted">5 hours ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">4 days ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-secondary">
                                            <i class="mdi mdi-heart"></i>
                                        </div>
                                        <p class="notify-details">Carlos Crouch liked
                                            <b>Admin</b>
                                            <small class="text-muted">13 days ago</small>
                                        </p>
                                    </a>
                                </div>
    
                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    Ver Todo
                                    <i class="fi-arrow-right"></i>
                                </a>
    
                            </div>
                        </li>
    
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="../../assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    <?php echo 'Administrador';?> <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Bienvenido !</h6>
                                </div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>Mi Cuenta</span>
                                </a>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-settings"></i>
                                    <span>Configuraciones</span>
                                </a>

                                <div class="dropdown-divider"></div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Cerrar Sesion</span>
                                </a>
    
                            </div>
                        </li>    
                    </ul>
    
                    <!-- LOGO -->
                    <div class="logo-box">
                        <!-- <a href="index.html" class="logo text-center">
                            <span class="logo-lg">
                                <img src="../../assets/images/logo-light.png" alt="" height="16">
                            </span>
                            <span class="logo-sm">
                                <img src="../../assets/images/logo-sm.png" alt="" height="24">
                            </span>
                        </a> -->
                    </div>
    
                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu active">
                                <a href="index.html"><i class="mdi mdi-view-dashboard"></i>Dashboard</a>
                            </li>
							<li class="has-submenu">
                                <a href="monitor/"><i class="mdi mdi-monitor-dashboard"></i>Monitor</a>
                            </li>
							<li class="has-submenu">
                                <a href="establecimientos/"><i class="mdi mdi-home-group"></i>Establecimientos</a>
                            </li>
							<!--
							<li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-lifebuoy"></i>Curriculum <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Editar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Eliminar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                        
                                </ul>
                            </li>
                            
                            <li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-home-group"></i>Establecimiento <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="mdi mdi-account-multiple-outline"></i>Usuarios <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Editar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Eliminar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>
							-->
                            
							<li class="has-submenu">
                                
                            </li>

                            <li class="has-submenu">
                                
                            </li>

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sistema Educacional</a></li>
                                    <li class="breadcrumb-item active">Dashboard</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title --> 

                <div class="row">

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Establecimientos</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Usuarios</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Planificaciones</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Evaluaciones</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                </div>
                <!-- end row -->

                <div class="row">
                    <div class="col-xl-4">
                        <div class="card-box">
                            
                            <h4 class="header-title mt-0">Rendimiento por establecimiento</h4>

                            <div class="widget-chart text-center">
                                <div id="morris-donut-example" style="height: 245px;" class="morris-chart" dir="ltr"></div>
								<ul class="list-inline chart-detail-list">
                                    <li class="list-inline-item"></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-xl-4">
                        <div class="card-box">
                            <h4 class="header-title mt-0">Comparativa Anual</h4>
                            <div id="morris-bar-example" style="height: 280px;" class="morris-chart" dir="ltr"></div>
                        </div>
                    </div><!-- end col -->

                    <div class="col-xl-4">
                        <div class="card-box">
                            <h4 class="header-title mt-0">Proyección Educacional</h4>
                            <div id="morris-line-example" style="height: 280px;" class="morris-chart" dir="ltr"></div>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->      

            </div> <!-- end container -->
        </div>
		<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog modal-dialog-centered modal-xl">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myCenterModalLabel">Ayuda - Modulo Administrador</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					</div>
					<div class="modal-body">
						<div class="col-xl-12">
							<div id="accordion">
								<div class="card mb-0">
									<div class="card-header" id="headingOne">
										<h5 class="m-0">
											<a href="#collapseOne" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
												Dashboard
											</a>
										</h5>
									</div>
						
									<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
										<div class="card-body">
											..
										</div>
									</div>
								</div>
								<div class="card mb-0">
									<div class="card-header" id="headingTwo">
										<h5 class="m-0">
											<a href="#collapseTwo" class="collapsed text-dark" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
												Monitor
											</a>
										</h5>
									</div>
									<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
										<div class="card-body">
											..
										</div>
									</div>
								</div>
								<div class="card mb-0">
									<div class="card-header" id="headingThree">
										<h5 class="m-0">
											<a href="#collapseThree" class="collapsed text-dark" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
												Curriculum
											</a>
										</h5>
									</div>
									<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
										<div class="card-body">
											..
										</div>
									</div>
								</div>
								<div class="card mb-0">
									<div class="card-header" id="headingThree">
										<h5 class="m-0">
											<a href="#collapseFour" class="collapsed text-dark" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFour">
												Establecimientos
											</a>
										</h5>
									</div>
									<div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
										<div class="card-body">
											..
										</div>
									</div>
								</div>
								<div class="card mb-0">
									<div class="card-header" id="headingThree">
										<h5 class="m-0">
											<a href="#collapseFive" class="collapsed text-dark" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFive">
												Usuarios
											</a>
										</h5>
									</div>
									<div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
										<div class="card-body">
											..
										</div>
									</div>
								</div>
							</div>
							<br>
							<div>
								<button class="btn btn-success waves-effect waves-light"> <i class=" fas fa-angle-down"></i> <span>Descargar Manual del Usuario</span> </button>      <button class="btn btn-success waves-effect waves-light"> <i class=" fas fa-angle-down"></i> <span>Manual del Sistema</span> </button>
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        2016 - 2019 &copy; Adminto theme by <a href="">Coderthemes</a> 
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="../../assets/js/vendor.min.js"></script>

        <!-- knob plugin -->
        <script src="../../assets/libs/jquery-knob/jquery.knob.min.js"></script>

        <!--Morris Chart-->
        <script src="../../assets/libs/morris-js/morris.min.js"></script>
        <script src="../../assets/libs/raphael/raphael.min.js"></script>

        <!-- Dashboard init js-->
        <script src="../../assets/js/pages/dashboard.init.js"></script>

        <!-- App js-->
        <script src="../../assets/js/app.min.js"></script>
        
    </body>
</html>