<?php
//include('../../../conexion/conexion.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tarea1 - Taller de integración de software</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="../../../assets/images/favicon.ico">

        <!-- App css -->
		<link href="../../..assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../../../assets/css/app.min.css" rel="stylesheet" type="text/css" />
		<link href="../../../assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
    
                        <li class="d-none d-sm-block">
                            <form class="app-search">
                                <div class="app-search-box">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar...">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fe-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
            
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">9</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
    
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-dark">
                                                <small>Limpiar Todo</small>
                                            </a>
                                        </span>Notificaciones
                                    </h5>
                                </div>
    
                                <div class="slimscroll noti-scroll">
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon">
                                            <img src="../../../assets/images/users/user-1.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Cristina Pride</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Hi, How are you? What about our next meeting</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-primary">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">1 min ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon">
                                            <img src="../../../assets/images/users/user-4.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Karen Robinson</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Wow ! this admin looks good and awesome design</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning">
                                            <i class="mdi mdi-account-plus"></i>
                                        </div>
                                        <p class="notify-details">New user registered.
                                            <small class="text-muted">5 hours ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">4 days ago</small>
                                        </p>
                                    </a>
    
                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-secondary">
                                            <i class="mdi mdi-heart"></i>
                                        </div>
                                        <p class="notify-details">Carlos Crouch liked
                                            <b>Admin</b>
                                            <small class="text-muted">13 days ago</small>
                                        </p>
                                    </a>
                                </div>
    
                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    Ver Todo
                                    <i class="fi-arrow-right"></i>
                                </a>
    
                            </div>
                        </li>
    
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="../../../assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    <?php echo 'Administrador';?> <i class="mdi mdi-chevron-down"></i> 
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Bienvenido !</h6>
                                </div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>Mi Cuenta</span>
                                </a>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-settings"></i>
                                    <span>Configuraciones</span>
                                </a>

                                <div class="dropdown-divider"></div>
    
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Cerrar Sesion</span>
                                </a>
    
                            </div>
                        </li>    
                    </ul>
    
                    <!-- LOGO -->
                    <div class="logo-box">
                        <!-- <a href="index.html" class="logo text-center">
                            <span class="logo-lg">
                                <img src="../../../assets/images/logo-light.png" alt="" height="16">
                            </span>
                            <span class="logo-sm">
                                <img src="../../../assets/images/logo-sm.png" alt="" height="24">
                            </span>
                        </a> -->
                    </div>
    
                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="index.html"><i class="mdi mdi-view-dashboard"></i>Dashboard</a>
                            </li>

							<li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-lifebuoy"></i>Curriculum <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Editar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic.html">Eliminar Curriculum</a>
                                            </li>
                                        </ul>
                                    </li>
                        
                                </ul>
                            </li>
                            
                            <li class="has-submenu">
                                <a href="#">
                                    <i class="mdi mdi-home-group"></i>Establecimiento <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Establecimiento</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu active">
                                <a href="#"> <i class="mdi mdi-account-multiple-outline"></i>Usuarios <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#">Agregar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Nuevo Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Editar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Editar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
									<li class="has-submenu">
                                        <a href="#">Eliminar <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="form-elements.html">Eliminar Usuario</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                
                            </li>

                            <li class="has-submenu">
                                
                            </li>

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sistema Educacional</a></li>
                                    <li class="breadcrumb-item">Administrador</li>
									<li class="breadcrumb-item active">Usuarios</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Registro de Usuarios</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title --> 

                <div class="row">

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Directores</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Profesores</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                            data-bgColor="#F9B9B9" value="58"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>

                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 256 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Estudiantes</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                    <div class="col-xl-3 col-md-6">
                        <div class="card-box">
                            <h4 class="header-title mt-0 mb-4">Total Apoderados</h4>

                            <div class="widget-chart-1">
                                <div class="widget-chart-box-1 float-left" dir="ltr">
                                    <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#ffbd4a"
                                            data-bgColor="#FFE6BA" value="80"
                                            data-skin="tron" data-angleOffset="180" data-readOnly=true
                                            data-thickness=".15"/>
                                </div>
                                <div class="widget-detail-1 text-right">
                                    <h2 class="font-weight-normal pt-2 mb-1"> 4569 </h2>
                                    <p class="text-muted mb-1">Registro a la fecha</p>
                                </div>
                            </div>
                        </div>

                    </div><!-- end col -->

                </div>
                <!-- end row -->

                <div class="row">
					<div class="col-12">
                        <div class="card-box table-responsive">
                            <h4 class="mt-0 header-title">Lista de Usuarios</h4>
                            <p class="text-muted font-14 mb-3"><button class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target="#add_usuario"> <i class="fa fa-plus mr-1"></i> <span>Usuario</span> </button></p>

                            <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>RUT</th>
                                    <th>Nombre</th>
                                    <th>A. Paterno</th>
                                    <th>A. Materno</th>
                                    <th>E-Mail</th>
									<th>Fecha Creación</th>
									<th>Herramientas</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
									<td></td>
									<td><button class="btn btn-icon waves-effect waves-light btn-primary" data-toggle="modal" data-target="#edit_usuario"> <i class="fas fa-edit"></i> </button>  	<button class="btn btn-icon waves-effect waves-light btn-danger" data-toggle="modal" data-target="#del_usuario"> <i class="fas fa-times"></i> </button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div id="add_usuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="myModalLabel">Agregar Usuario</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							</div>
							<div class="modal-body">
								<form action="#" data-parsley-validate novalidate>
								<div class="form-group">
									<label for="txt_rut">RUT *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el RUT" class="form-control" id="txt_rut">
								</div>
								<div class="form-group">
									<label for="userName">Nombre *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el nombre" class="form-control" id="userName">
								</div>
								<div class="form-group">
									<label for="userAP">Apellido Paterno *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el apellido paterno" class="form-control" id="userAP">
								</div>
								<div class="form-group">
									<label for="userAM">Apellido Materno *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el apellido materno" class="form-control" id="userAM">
								</div>
								<div class="form-group">
									<label for="emailAddress">E-Mail*</label>
									<input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control" id="emailAddress">
								</div>
								<div class="form-group">
									<label for="pass1">Contraseña*</label>
									<input id="pass1" type="password" placeholder="Password" required class="form-control">
								</div>
								<div class="form-group">
									<label for="passWord2">Confirmar Contraseña*</label>
									<input data-parsley-equalto="#pass1" type="password" required placeholder="Password" class="form-control" id="passWord2">
								</div>
								<div class="form-group">
									<label for="tipoUser">Tipo Usuario</label>
									<select class="form-control select2">
										<option>Selecciene...</option>
										<option>Administrador</option>
										<option>Director</option>
										<option>Profesor</option>
										<option>Alumno</option>
										<option>Apoderado</option>
									</select>
								</div>
										
											
								<p align="center" class="text-muted" id="msgerror"></p>
								<div class="form-group text-right mb-0">
									<button id="btnvalida" class="btn btn-primary waves-effect waves-light mr-1">Guardar</button>
								</div>
								</form>
							</div>
							<div class="modal-footer"></div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
				<div id="edit_usuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							</div>
							<div class="modal-body">
								<form action="#" data-parsley-validate novalidate>
								<div class="form-group">
									<label for="txt_rut">RUT *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el RUT" class="form-control" id="txt_rut" value="5915818-K">
								</div>
								<div class="form-group">
									<label for="userName">Nombre *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el nombre" class="form-control" id="userName" value="Daniel">
								</div>
								<div class="form-group">
									<label for="userAP">Apellido Paterno *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el apellido paterno" class="form-control" id="userAP" value="Tapia">
								</div>
								<div class="form-group">
									<label for="userAM">Apellido Materno *</label>
									<input type="text" name="nick" parsley-trigger="change" required placeholder="Ingrese el apellido materno" class="form-control" id="userAM" value="Bazan">
								</div>
								<div class="form-group">
									<label for="emailAddress">E-Mail*</label>
									<input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control" id="emailAddress" value="daniel.tapia@gmail.com">
								</div>
								<div class="form-group">
									<label for="pass1">Contraseña*</label>
									<input id="pass1" type="password" placeholder="Password" required class="form-control">
								</div>
								<div class="form-group">
									<label for="passWord2">Confirmar Contraseña*</label>
									<input data-parsley-equalto="#pass1" type="password" required placeholder="Password" class="form-control" id="passWord2">
								</div>
								<div class="form-group">
									<label for="tipoUser">Tipo Usuario</label>
									<select class="form-control select2">
										<option>Selecciene...</option>
										<option>Administrador</option>
										<option selected>Director</option>
										<option>Profesor</option>
										<option>Alumno</option>
										<option>Apoderado</option>
									</select>
								</div>
										
											
								<p align="center" class="text-muted" id="msgerror"></p>
								<div class="form-group text-right mb-0">
									<button id="btnvalida" class="btn btn-primary waves-effect waves-light mr-1">Guardar</button>
								</div>
								</form>
							</div>
							<div class="modal-footer"></div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
                <div id="del_usuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="myCenterModalLabel">Desea eliminar al siguiente usuario</h4>
							</div>
							<div class="modal-body">
								<p><strong>Usuario:</strong> Jean Tapia Rodriguez</p>
								<button class="btn btn-info waves-effect waves-light"> <i class="fas fa-check mr-1"></i> <span>Si</span> </button> <button class="btn btn-danger waves-effect waves-light"> <i class="fas fa-times"></i> <span>No</span> </button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
				<!-- end row -->      

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        2016 - 2019 &copy; Adminto theme by <a href="">Coderthemes</a> 
                    </div>
                </div>
            </div>
        </footer>
		
        <!-- end Footer -->
		<!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
		
		<script src="../../../assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.select.min.js"></script>
			
        <!-- Vendor js -->
        <script src="../../../assets/js/vendor.min.js"></script>
		
		<!-- third party js -->
        <script src="../../../assets/libs/datatables/jquery.dataTables.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.bootstrap4.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="../../../assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.buttons.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.html5.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.flash.min.js"></script>
        <script src="../../../assets/libs/datatables/buttons.print.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.keyTable.min.js"></script>
        <script src="../../../assets/libs/datatables/dataTables.select.min.js"></script>
        <script src="../../../assets/libs/pdfmake/pdfmake.min.js"></script>
        <script src="../../../assets/libs/pdfmake/vfs_fonts.js"></script>
		
        <!-- knob plugin -->
        <script src="../../../assets/libs/jquery-knob/jquery.knob.min.js"></script>

        <!-- Dashboard init js-->
        <!-- <script src="../../../assets/js/pages/dashboard.init.js"></script> -->
		<script>
			$(document).ready(function(){$("#datatable").DataTable();var a=$("#datatable-buttons").DataTable({lengthChange:!1,buttons:["copy","excel","pdf"]});$("#key-table").DataTable({keys:!0}),$("#responsive-datatable").DataTable(),$("#selection-datatable").DataTable({select:{style:"multi"}}),a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)")});	
		</script>
        <!-- App js-->
        <script src="../../../assets/js/app.min.js"></script>
		<script>
				$(document).ready(function() {
					$('#guardar').click(function(){
						if($("#email").val().indexOf('@', 0) == -1 || $("#email").val().indexOf('.', 0) == -1) {
							alert('El correo electrónico introducido no es correcto.');
							return false;
						}

						alert('El email introducido es correcto.');
					});
				});


			
				var Fn = {
				// Valida el rut con su cadena completa "XXXXXXXX-X"
				validaRut : function (rutCompleto) {
					rutCompleto = rutCompleto.replace("‐","-");
					if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
						return false;
					var tmp 	= rutCompleto.split('-');
					var digv	= tmp[1]; 
					var rut 	= tmp[0];
					if ( digv == 'K' ) digv = 'k' ;
					
					return (Fn.dv(rut) == digv );
				},
				dv : function(T){
					var M=0,S=1;
					for(;T;T=Math.floor(T/10))
						S=(S+T%10*(9-M++%6))%11;
					return S?S-1:'k';
				}
			}


			$("#btnvalida").click(function(){
				
				if (Fn.validaRut( $("#txt_rut").val() )){
					$('#userName').focus();
					$("#msgerror").html("");
				} else if($("#txt_rut").val()==''){
					alert("Favor ingrese su rut");
					$('#txt_rut').focus();
					$("#msgerror").html("");
				}else{
					$("#msgerror").html("El RUT ingresado no es válido");
				}
			});
			
		</script>
    </body>
</html>